﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BridgeCrossing
{
    class Program
    {
        public enum Direction { Outbound, Return }

        class Move
        {
            public Move(Direction direction, IEnumerable<Traveller> travellers)
            {
                this.direction = direction;
                this.travellers = travellers;
            }

            public override string ToString()
            {
                return string.Join(" & ", travellers.OrderBy(t => t.crossingTime)) + (direction == Direction.Outbound ? " out" : " return");
            }

            Direction direction;
            IEnumerable<Traveller> travellers;
        }

        class Traveller
        {
            public Traveller(int crossingTime, string name = null)
            {
                this.crossingTime = crossingTime;
                this.name = name;
            }

            public override string ToString()
            {
                if (string.IsNullOrWhiteSpace(name))
                    return crossingTime.ToString();
                else
                    return string.Format("{0}[{1}]", name, crossingTime);
            }

            public readonly int crossingTime;
            public readonly string name;
        }

        class Solution
        {
            public Solution(int totalTime, List<Move> moves)
            {
                this.totalTime = totalTime;
                this.moves = moves;
            }

            public override string ToString()
            {
                return string.Format("[{0}] {1}", totalTime, string.Join(", ", moves));
            }

            public readonly int totalTime;
            public readonly List<Move> moves;

        }

        static void Main(string[] args)
        {
            HashSet<Solution> solutions = new HashSet<Solution>();
            List<Move> currentSolution = new List<Move>();
            List<Traveller> near = new List<Traveller>();
            List<Traveller> far = new List<Traveller>();

            foreach(string arg in args)
            {
                int crossingTime;
                if (int.TryParse(arg, out crossingTime))
                    near.Add(new Traveller(crossingTime));
            }

            if (near.Count == 0)
            {
                Console.WriteLine("Usage");
                Console.WriteLine();
                Console.WriteLine("  {0} crossingTime crossingTime ...", Path.GetFileName(Assembly.GetEntryAssembly().Location));
                Console.WriteLine();
                return;
            }

            Explore(near, far, false, 0, currentSolution, solutions);

            foreach (Solution s in solutions.OrderBy(s => s.totalTime))
                Console.WriteLine(s);
        }

        static void Explore(List<Traveller> near, List<Traveller> far, bool lanternFar, int currentTotal, List<Move> currentMoves, ICollection<Solution> solutions)
        {
            if (near.Count == 0)
            {
                solutions.Add(new Solution(currentTotal, currentMoves));
                return;
            }

            if (lanternFar)
            {
                for(int i = 0; i < far.Count; i++)
                {
                    Traveller t = far[i];
                    Explore(near.With(t), far.Without(t), !lanternFar, currentTotal + t.crossingTime, currentMoves.With(new Move(Direction.Return, new[] { t })), solutions);
                }
            }
            else
            {
                for(int i = 0; i < near.Count; i++)
                {
                    Traveller t1 = near[i];
                    for (int j = i + 1; j < near.Count; j++)
                    {
                        Traveller t2 = near[j];
                        Explore(near.Without(t1, t2), far.With(t1, t2), !lanternFar, currentTotal + Math.Max(t1.crossingTime, t2.crossingTime), currentMoves.With(new Move(Direction.Outbound, new[] { t1, t2 })), solutions);
                    }
                }
            }
        }
    }

    public static class Extensions
    {
        public static T With<T, U>(this T s, params U[] u) where T : ICollection<U>, new()
        {
            T newCollection = new T();
            foreach (U uu in s)
                newCollection.Add(uu);
            foreach (U uu in u)
                newCollection.Add(uu);
            return newCollection;
        }

        public static T Without<T, U>(this T s, params U[] u) where T : ICollection<U>, new()
        {
            T newCollection = new T();
            foreach (U uu in s)
                newCollection.Add(uu);
            foreach (U uu in u)
                newCollection.Remove(uu);
            return newCollection;
        }
    }
}
